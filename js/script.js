function resize(){

}

$(document).ready(function(){

	$('header span.menu-icon').click(function(){
		$('header nav.mobile-menu').slideDown(function(){
			$('header span.close-icon').show()
		})
	});

	$('header span.close-icon').click(function(){
		$('header nav.mobile-menu').slideUp(function(){
			$('header span.close-icon').hide()
		})
	});

	$('header nav.mobile-menu ul li a.home').click(function(){
		$('nav.mobile-menu').slideUp(function(){
			$('header span.close-icon').hide()
		})
	})

	$('header nav.mobile-menu ul li a.team').click(function(){
		$('nav.mobile-menu').slideUp(function(){
			$('header span.close-icon').hide()
		})
	})

	$('header nav.mobile-menu ul li a.contacts').click(function(){
		$('nav.mobile-menu').slideUp(function(){
			$('header span.close-icon').hide()
		})
	})

	$('header nav.mobile-menu ul li a.events').click(function(){
		$('nav.mobile-menu').slideUp(function(){
			$('header span.close-icon').hide()
		})
	})

	var wow = new WOW(
      {
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       true,       // trigger animations on mobile devices (default is true)
        live:         true,       // act on asynchronously loaded content (default is true)
        callback:     function(box) {
          // the callback is fired every time an animation is started
          // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null // optional scroll container selector, otherwise use window
      }
    );
    wow.init();

    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                  scrollTop: target.offset().top
                }, 1750);
                return false;
            }
        }
    });
    resize();
});
  
$(window).resize(function(){

    resize();
});

$(window).on('load',function(){
    resize();
});


